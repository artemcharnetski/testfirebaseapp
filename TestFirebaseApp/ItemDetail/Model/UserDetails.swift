//Copyright © 2020 ac. All rights reserved.

import Foundation

struct UserDetails: Codable {
    let id: String
    let email: String
    let name: String
    let phone: String
    let username: String
    let website: String
}
