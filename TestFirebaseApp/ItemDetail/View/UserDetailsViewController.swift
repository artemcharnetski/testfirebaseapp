//Copyright © 2020 ac. All rights reserved.

import UIKit
import RxSwift
import RxCocoa

final class UserDetailsViewController: UIViewController {
    
    // MARK: - Constnats

    private struct Constants {
        static let margin = CGFloat(20)
    }
    
    // MARK: - Properties
    
    private let viewModel: UserDetailsViewModel
    private var disposeBag = DisposeBag()
    private let stackView = ViewBuilders.createStackView()
    private let rightBarButton = UIBarButtonItem(title: L10n.Generic.delete, style: .plain, target: self, action: nil)
    
    // MARK: - Initialization
    
    init(viewModel: UserDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.loadDetails()
    }

    // MARK: - Setup
    
    private func setup() {
        setupSubviews()
        setupBinding()
        setupObserving()
    }
    
    private func setupSubviews() {
        view.backgroundColor = .white
        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.margin),
            stackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: Constants.margin),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: Constants.margin),
            stackView.bottomAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    private func setupDetails(_ userDetails: UserDetails) {
        let idLabel = UILabel()
        idLabel.text = "\(L10n.User.id): \(userDetails.id)"
        stackView.addArrangedSubview(idLabel)
        let nameLabel = UILabel()
        nameLabel.text = "\(L10n.User.name): \(userDetails.name)"
        stackView.addArrangedSubview(nameLabel)
        let usernameLabel = UILabel()
        usernameLabel.text = "\(L10n.User.username): \(userDetails.username)"
        stackView.addArrangedSubview(usernameLabel)
        let emailLabel = UILabel()
        emailLabel.text = "\(L10n.User.email): \(userDetails.email)"
        stackView.addArrangedSubview(emailLabel)
        let phoneLabel = UILabel()
        phoneLabel.text = "\(L10n.User.phone): \(userDetails.phone)"
        stackView.addArrangedSubview(phoneLabel)
        let websiteLabel = UILabel()
        websiteLabel.text = "\(L10n.User.website): \(userDetails.website)"
        stackView.addArrangedSubview(websiteLabel)
    }
    
    private func setupBinding() {
        viewModel.userDetail.drive(onNext: { [weak self] (userDetails) in
            guard let userDetails = userDetails else { return }
            self?.setupDetails(userDetails)
        })
            .disposed(by: disposeBag)
        viewModel.error.drive(onNext: { [weak self] (errorMessage) in
            self?.presentError(title: L10n.Errors.title,
                               message: errorMessage,
                               dismissButtonTitle: L10n.Generic.ok)
        })
            .disposed(by: disposeBag)
    }
    
    private func setupObserving() {
        rightBarButton.rx.tap.subscribe { [weak self] _ in
            self?.viewModel.removeUser()
        }
        .disposed(by: disposeBag)
    }
    
    // MARK: - Private
    
    private func presentError(title: String, message: String, dismissButtonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: dismissButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true)
    }

}
