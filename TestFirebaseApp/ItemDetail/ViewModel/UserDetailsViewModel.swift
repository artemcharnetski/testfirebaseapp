//Copyright © 2020 ac. All rights reserved.

import Foundation
import RxSwift
import RxCocoa

final class UserDetailsViewModel {
    
    // MARK: - Properties

    private let apiClient = APIClient.shared
    private let appCoordinator = AppCoordinator.shared
    private let userDetailsSource = BehaviorRelay<UserDetails?>(value: nil)
    private(set) lazy var userDetail = userDetailsSource.asDriver()
    private(set) lazy var error = errorRelay.asDriver(onErrorJustReturn: (""))
    private let errorRelay = PublishRelay<String>()
    private let disposeBag = DisposeBag()
    let user: User

    // MARK: - Initialization
    
    init(user: User) {
        self.user = user
    }
    
    // MARK: - Public API
    
    func loadDetails() {
        apiClient.getUserDetails(user)
            .subscribe(onSuccess: { [userDetailsSource] (userDetails) in userDetailsSource.accept(userDetails) },
                       onError: { [errorRelay] (error) in errorRelay.accept(error.localizedDescription) })
            .disposed(by: disposeBag)
    }
    
    func removeUser() {
        apiClient.removeUser(id: user.id)
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [appCoordinator] in appCoordinator.coordinate(to: .usersList) },
                       onError: { [errorRelay] (error) in errorRelay.accept(error.localizedDescription) })
            .disposed(by: disposeBag)
    }
    
}
