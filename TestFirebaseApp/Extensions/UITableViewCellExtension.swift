//Copyright © 2020 ac. All rights reserved.

import UIKit

extension UITableViewCell {
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
