//Copyright © 2020 ac. All rights reserved.

import UIKit

extension UITableView {
    
    func registerClassForCell<T: UITableViewCell>(_ cellClass: T.Type) {
        self.register(cellClass, forCellReuseIdentifier: cellClass.identifier)
    }
    
    func registerNibForCell<T: UITableViewCell>(_ cell: T.Type) {
        let nib = UINib(nibName: String(describing: cell), bundle: nil)
        self.register(nib, forCellReuseIdentifier: cell.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for index: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.identifier, for: index) as! T
    }
    
}
