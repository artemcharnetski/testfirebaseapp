//Copyright © 2020 ac. All rights reserved.

import UIKit
import RxSwift
import RxCocoa

final class AppCoordinator {
    
    // MARK: - Inner types
    
    enum Targets {
        case usersList
        case userDetails(user: User)
    }
    
    // MARK: - Properties
    
    public static let shared = AppCoordinator()
    
    // MARK: - Initialization
    
    private init() {}
    
    // MARK: Private
    
    var navigationController = UINavigationController()
    
    // MARK: - API
    
    func coordinate(to target: Targets) {
        switch target {
        case .usersList:
            if let usersListViewController = navigationController.viewControllers.first(where: { $0 is UsersListViewController }) {
                navigationController.popToViewController(usersListViewController, animated: true)
            } else {
                let viewModel = UsersListViewModel()
                let usersListViewController = UsersListViewController(viewModel: viewModel)
                navigationController.pushViewController(usersListViewController, animated: true)
            }
        case .userDetails(let user):
            let viewModel = UserDetailsViewModel(user: user)
            let userDetailsViewController = UserDetailsViewController(viewModel: viewModel)
            navigationController.pushViewController(userDetailsViewController, animated: true)
        }
    }
    
}
