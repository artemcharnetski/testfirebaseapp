//Copyright © 2020 ac. All rights reserved.

import Foundation
import RxSwift
import RxCocoa

protocol APIClientProtocol {
    func getUsers() -> Single<[User]>
    func getUserDetails(_ user: User) -> Single<UserDetails>
    func removeUser(id: String) -> Completable
}

final class APIClient: APIClientProtocol {
    
    // MARK: - Constnats

    private struct Constants {
        static let baseUrl = "https://testfirebase-bd69a.firebaseio.com/"
        static let usersEndpoint = "users.json"
        static func userDetailsEndpoint(userId: String) -> String {
            return Constants.baseUrl + "users/\(userId).json"
        }
    }
    
    // MARK: - Inner types
    
    public enum HTTPMethod: String {
        case get = "GET"
        case put = "PUT"
        case post = "POST"
        case delete = "DELETE"
        case head = "HEAD"
        case options = "OPTIONS"
        case trace = "TRACE"
        case connect = "CONNECT"
    }
    
    // MARK: - Properties
    
    public static let shared = APIClient()
    private init() {}
    
    // MARK: - Public API
    
    func getUsers() -> Single<[User]> {
        let stringUrl = Constants.baseUrl + Constants.usersEndpoint
        guard let url = URL(string: stringUrl) else {
            return .error(apiError("Invalid Url."))
        }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        return URLSession.shared.rx.response(request: request)
            .map { result -> Data in
                guard result.response.statusCode == 200 else {
                    throw self.apiError("Invalid response.")
                }
                return result.data
            }.map { data in
                guard let users = try? JSONDecoder().decode([String: User].self, from: data) else { return [] }
                return Array(users.values).sorted(by: { $0.id > $1.id })
            }
            .asSingle()
    }
    
    func getUserDetails(_ user: User) -> Single<UserDetails> {
        let userId = user.id
        let stringUrl = Constants.userDetailsEndpoint(userId: userId)
        guard let url = URL(string: stringUrl) else {
            return .error(apiError("Invalid Url."))
        }
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        return URLSession.shared.rx.response(request: request)
            .map { result -> Data in
                guard result.response.statusCode == 200 else {
                    throw self.apiError("Invalid response.")
                }
                return result.data
            }.map { data in
                guard let userDetails = try? JSONDecoder().decode(UserDetails.self, from: data) else { throw self.apiError("JSON parse error.") }
                return userDetails
            }
            .asSingle()
    }
    
    func removeUser(id: String) -> Completable {
        let stringUrl = Constants.userDetailsEndpoint(userId: id)
        guard let url = URL(string: stringUrl) else {
            return .error(apiError("Invalid Url."))
        }
        return Completable.create { completable in
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.delete.rawValue
            let task = URLSession.shared.dataTask(with: request) { _, response, error in
                if let error = error {
                    completable(.error(error))
                } else {
                    if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                        completable(.completed)
                    } else {
                        completable(.error(self.apiError("Something went wrong.")))
                    }
                }
            }

            task.resume()

            return Disposables.create { task.cancel() }
        }
        
    }
    
    // MARK: - Private
    
    private func apiError(_ error: String) -> NSError {
        return NSError(domain: "FirebaseAPI", code: -1, userInfo: [NSLocalizedDescriptionKey: error])
    }
    
}
