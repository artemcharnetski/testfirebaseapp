// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Errors {
    /// Error
    internal static let title = L10n.tr("Localizable", "errors.title")
    /// Unable to retrieve user details.
    internal static let userDetails = L10n.tr("Localizable", "errors.userDetails")
    /// Unable to retrieve users.
    internal static let users = L10n.tr("Localizable", "errors.users")
  }

  internal enum Generic {
    /// Delete
    internal static let delete = L10n.tr("Localizable", "generic.delete")
    /// OK
    internal static let ok = L10n.tr("Localizable", "generic.ok")
  }

  internal enum User {
    /// Email
    internal static let email = L10n.tr("Localizable", "user.email")
    /// Id
    internal static let id = L10n.tr("Localizable", "user.id")
    /// Name
    internal static let name = L10n.tr("Localizable", "user.name")
    /// Phone
    internal static let phone = L10n.tr("Localizable", "user.phone")
    /// Username
    internal static let username = L10n.tr("Localizable", "user.username")
    /// Website
    internal static let website = L10n.tr("Localizable", "user.website")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
