//Copyright © 2020 ac. All rights reserved.

import Foundation

struct User: Codable {
    let id: String
    let email: String
}
