//Copyright © 2020 ac. All rights reserved.

import Foundation
import RxSwift
import RxCocoa

final class UsersListViewModel {
    
    // MARK: - Properties
    
    private let apiClient = APIClient.shared
    private let appCoordinator = AppCoordinator.shared
    private let usersSource = BehaviorRelay<[User]>(value: [])
    private(set) lazy var users = usersSource.asDriver()
    private(set) lazy var error = errorRelay.asDriver(onErrorJustReturn: (""))
    private let errorRelay = PublishRelay<String>()
    private let disposeBag = DisposeBag()
    
    // MARK: - Public API
    
    func loadData() {
        apiClient.getUsers()
            .subscribe(onSuccess: { [usersSource] (users) in usersSource.accept(users.compactMap { $0 }) },
                       onError: { [errorRelay] (error) in errorRelay.accept(error.localizedDescription) })
            .disposed(by: disposeBag)
    }
    
    func removeUser(_ user: User) {
        apiClient.removeUser(id: user.id)
            .subscribe(onCompleted: { [usersSource] in
                let filteredUsers = usersSource.value.filter({ $0.id != user.id })
                usersSource.accept(filteredUsers)
            }, onError: { [errorRelay] (error) in errorRelay.accept(error.localizedDescription) })
            .disposed(by: disposeBag)
    }
    
    func select(_ user: User) {
        appCoordinator.coordinate(to: .userDetails(user: user))
    }
    
}
