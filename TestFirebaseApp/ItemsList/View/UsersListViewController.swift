//Copyright © 2020 ac. All rights reserved.

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

final class UsersListViewController: UIViewController, UITableViewDelegate {

    // MARK: - Properties
    
    private let viewModel: UsersListViewModel
    private var disposeBag = DisposeBag()
    private let tableView = ViewBuilders.createTableView()
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, User>>(
      configureCell: { (_, tableView, indexPath, user) in
        let cell = tableView.dequeueReusableCell(for: indexPath)
        cell.textLabel?.text = user.email
        return cell
      }, canEditRowAtIndexPath: {_, _ in
          return true
      }
    )
    
    // MARK: - Initialization
    
    init(viewModel: UsersListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadData()
    }

    // MARK: - Setup
    
    private func setup() {
        setupSubviews()
        setupObserving()
        setupBinding()
    }
    
    private func setupSubviews() {
        view.backgroundColor = .white
        
        tableView.registerClassForCell(UITableViewCell.self)
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func setupBinding() {
        viewModel.users
            .map { [SectionModel(model: "", items: $0)] }
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        viewModel.error.drive(onNext: { [weak self] (errorMessage) in
        self?.presentError(title: L10n.Errors.title,
                           message: errorMessage,
                           dismissButtonTitle: L10n.Generic.ok)
        })
        .disposed(by: disposeBag)
    }
    
    private func setupObserving() {
        tableView.rx.modelDeleted(User.self)
            .subscribe(onNext: { [weak self] user in
                self?.viewModel.removeUser(user)
            })
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(User.self)
            .subscribe(onNext: { [weak self] user in
                self?.viewModel.select(user)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - Private
    
    private func presentError(title: String, message: String, dismissButtonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: dismissButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true)
    }

}
